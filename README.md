# Kubernetes

**Requirements:**

* install docker desktop.

**In docker desktop:**

1. In docker desktop go to "settings"-->"Kubernetes"-->"Enable Kubernetes"

2. Verify Kubernetes is Running:Open a terminal and run the following command to ensure that your Kubernetes cluster is running: 
```
kubectl cluster-info
```
3. create a YAML file to define Kubernetes namespaces and apply it using kubectl

4. To verify that the namespaces have been created, run the following command:
```
kubectl get namespaces
```
